# 2. Use Gitlab for Source Repo

Date: 2023-02-13

## Status

Accepted

## Context

Github is not available for on-premise.
Gitlab can be used in on-premise hosting.
Gitlab is open source.

## Decision

No decide Source Repo to Gitlab cloud.

## Consequences

We will use git cloud until MVP version.
Infra team will setup on-premise gitlab server.
They will migrate 
